# My very opiniated Configurations for Rector and ECS

## Installation

```bash
composer require --dev lubiana/code-quality
```

Add my Setlist to ECS in `ecs.php`:

```php
return static function (\Symplify\EasyCodingStandard\Config\ECSConfig $config): void {
    $config->sets([
        \Lubiana\CodeQuality\LubiSetList::ECS,
    ]);
};
```

And for rector.php:

```php
return static function (\Rector\Config\RectorConfig $config): void {
    $config->sets([
        \Lubiana\CodeQuality\LubiSetList::RECTOR,
    ]);
};
```



