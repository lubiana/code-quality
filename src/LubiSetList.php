<?php declare(strict_types=1);

namespace Lubiana\CodeQuality;

final class LubiSetList
{
    public const string RECTOR = __DIR__ . '/../config/rector.php';
    public const string ECS = __DIR__ . '/../config/ecs.php';
}
