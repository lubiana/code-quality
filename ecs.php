<?php declare(strict_types=1);

use Lubiana\CodeQuality\LubiSetList;
use Symplify\EasyCodingStandard\Config\ECSConfig;

return ECSConfig::configure()
    ->withSets([LubiSetList::ECS])
    ->withPaths(
        [
            __DIR__ . '/src',
            __DIR__ . '/config',
        ]
    )->withRootFiles();
