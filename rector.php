<?php declare(strict_types=1);

use Lubiana\CodeQuality\LubiSetList;
use Rector\Config\RectorConfig;

return RectorConfig::configure()
    ->withPaths([
        __DIR__ . '/src',
        __DIR__ . '/config',
    ])
    ->withSets([LubiSetList::RECTOR])
    ->withRootFiles()
;
